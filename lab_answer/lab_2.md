1. Apakah perbedaan antara JSON dan XML?
XML :
Struktur data dalam XML berbentuk tree. Informasi didalam XML dibungkas didalam beberapa tag (Markup Language) yang disebut 'branch' dan 'leaves' dan mengandung 'root' atau parent element yang merupakan nama objeknya. XML file tidak mendukung penggunaan array. XML merupakan extension dari SGML (Standard Generalized Markup Language). Pemrosesan data pada XML lebih secure tetapi kecepatan prosesnya lebih rendah

JSON :
Struktur data JSON berbentuk Map atau dictionary dalam python. Informasi didalam file JSON disajikan dalam bentuk 'key' dan 'value' di dalam map tersebut dan informasi tersebut direpresentasikan sebagai objek. JSON mendukung penggunaan array. JSON merupakan extension Java Script. Data pada JSON lebih mudah di parsing tetapi lebih tidak aman daripada XML

Source : https://www.educba.com/json-vs-xml/, https://www.w3schools.com/js/js_json_xml.asp, https://www.geeksforgeeks.org/difference-between-json-and-xml/

2. Apakah perbedaan antara HTML dan XML?
XML : 
XML memberikan framework untuk mendefinisikan Markup Language. XMl bersifat dinamis karena berfokus pada transfer dan penyimpanan data. XML bersifat case-sensitive. Tag dari XML didefinisikan oleh data yang diproses (sehingga extensible, artinya jumlahnya bisa ditambah terus sesuai kebutuhan) dan sangat strict dalam menutup tag.

HTML :
HTML merupakan suatu Markup Language yang didalamnya terdapat fungsi Hyper Text (link). HTML bersifat statik karena menghandle penyajian (tampilan) data yang sudah ada pada client. HTML tidak case-sensitive (misal : <table> == <Table>). Tag dari HTML sudah terdefinisi (artinya tag html berjumlah terbatas) dan tidak strict dalam penutupan tag (tag yang tidak tertutup tidak akan membuat error).

Source : https://www.geeksforgeeks.org/html-vs-xml/, https://www.guru99.com/xml-vs-html-difference.html