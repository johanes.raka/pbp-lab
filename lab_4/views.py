from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http import HttpResponseRedirect
# Create your views here.
def index(request):
    note = Note.objects.all().values()
    response = {'note': note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    response = {'form': form}
    if (request.method == 'POST'):
        if(form.is_valid()):
            form.save()
            return HttpResponseRedirect('/lab-4')
    return render(request, "lab4_form.html", response)

def note_list(request):
    note = Note.objects.all().values()
    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)